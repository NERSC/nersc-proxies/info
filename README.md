# NERSC proxies

The goal of the NERSC-proxies project is to facilitate engagements with vendors and programming model research with kernels, microbenchmarks, and mini-applications that are representative of the NERSC workload.

The suite is hosted at https://gitlab.com/NERSC/nersc-proxies where each proxy is a separate repository.

## Guidelines

### licenses

While each proxy can have a different license, LBL BSD style licenses are preferred

* Some proxies developed outside LBL are OK
* If you created one, get a LBL modified BSD license via IPO office

### versions

Multiple versions of the same proxy are encouraged.

* at least one version should be GPU enabled
* at least one version should not be vendor specific (ie CUDA)
* Preferred (but not required):
   * Native: CUDA or HIP for GPU targets
       * Nominally best performance
       * Assess roofline targets
   * Directive based: OpenMP offload or OpenACC
       * A must for Fortran
   * Modern C++: e.g. Kokkos, SYCL, DPC++, etc
* ML/DL preferred:
     * Frameworks: Tensorflow, PyTorch, Julia
     * Microbenchmarks: xxDNN libraries with C/C++ wrappers

### build systems

No restrictions on languages or build systems

### Keep it simple

* avoid extra dependencies
* avoid complicated build systems

### Required Documentation

* Include build & run instructions
* Explain what the code is a proxy for/ what science problem it is solving
* Figure of Merit
  * What metrics matter for this application?
  * How should performance between different versions be compared?

### Self-contained

provide any needed inputs or an input generator

### Verification

Each proxy should have some mechanism to validate correctness of any outputs.

* preferred: required precision/ digits/ # of correct pixels ie more than a diff

## How to contribute

1. join the #nersc-proxies channel on NERSC's internal slack (if you have access)
   * tell us about your app!
1. Create new repository under gitlab.com/NERSC/nersc-proxies/<your proxy> 
   * if the code is new the repository should be _private_
   * consider _not_ using branches to manage multiple versions
1. Work with the team to review for consistentcy with the suite
1. File ipo.lbl.gov paperwork for licenses
1. Report issues to help.nersc.gov (mention that the request should be routed to Brandon Cook)
